FROM python
WORKDIR /app
COPY . /app
# Install pip requirements
RUN python -m pip install -r requirements.txt
# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
ENTRYPOINT [ "python" ]
CMD ["helloworld.py"]
